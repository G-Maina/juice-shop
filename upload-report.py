import requests
import sys

file_name = sys.argv[1]
scan_type = ''

if file_name == 'gitleaks.json':
    scan_type = 'gitleaks Scan'
elif file_name == 'njsscan.sarif':
    scan_type = 'SARIF'
elif file_name == 'semgrep.json':
    scan_type = 'Semgrep JSON Report'
elif file_name == 'retire.json':
    scan_type = 'Retire.js Scan'
elif file_name == 'trivy.json':
    scan_type = 'Trivy Scan'



headers = {
    'Authorization': 'Token 0f48e81fdfb5b575e54a700013e4a6b42e6ac249'
}

url = 'http://146.190.45.193:8080/api/v2/import-scan/'

data = {
    'active': True,
    'verified': True,
    'scan-type': scan_type,
    'minimum_severity': 'Low',
    'engagement': 3
}

files = {
    'file': open(file_name, 'rb')
}

response =requests.post(url, headers=headers, data=data, files=files)

if response.status_code == 201:
    print('Scan results imported successfully')
else:
    print('Failed to import scan results: {response.content')

